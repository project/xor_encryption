This is a light-weight XOR encryption API for Drupal. It is intended only to
address cases where you need encode information in a way that is reasonably
difficult for most users to discern.

For example, a frequent use case for this module is in encrypting entity IDs
in a "code" or "invitation link" that users will key-in or receive.

DO NOT USE THIS API TO ENCRYPT HIGHLY SENSITIVE INFORMATION.

The XOR ciper used in this module is likely trivial for someone experienced in
cryptography to crack. The longer and more unique the key you use, the more
secure the information will be, but if a user is exposed to multiple values
encrypted with the same key, he or she could use frequency analysis to discern
the key.
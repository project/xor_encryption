<?php
/**
 * @file
 *   The native PHP-based implementation of the exclusive-OR encryption
 *   provider.
 *
 *   This provider is only used as a last resort, since it cannot deal with
 *   large numbers. Consequently, it only functions on 64-bit PHP (it is pretty
 *   much useless on 32-bit).
 *
 * @author Guy Paddock (guy.paddock@redbottledesign.com)
 */
class NativePhpEncryptor extends XorEncryptor {
  /**
   * Initialize a new instance of NativePhpEncryptor for the specified number
   * base / radix, number of padding digits, and encryption key.
   *
   * @param integer $radix
   *   The "base" or radix of numbers (i.e. base-10, base-16, etc) that will be
   *   encrypted. If unset, the default of DEFAULT_RADIX will be a used.
   * @param integer $numPadDigits
   *   The number of random padding digits to add to an encrypted string to make
   *   it more difficult to derive the key. If unset, the default of
   *   DEFAULT_NUM_PAD_DIGITS will be a used.
   * @param number $key
   *   The key to use for encryption. If unset, a default of (PI * e) ^ 10 will
   *   be used.
   */
  public function __construct($radix = XorEncryptor::DEFAULT_RADIX,
      $numPadDigits = XorEncryptor::DEFAULT_NUM_PAD_DIGITS,
      $key = NULL) {
    parent::__construct($radix, $numPadDigits, $key);
  }

  /**
   * See {@link XorEncryptor#isSupported()}.
   */
  public function isSupported() {
    return ($this->detectPlatformPrecision() == 64);
  }

  /**
   * See {@link XorEncryptor#getName()}.
   */
  public function getName() {
    return t('Native PHP (low precision)');
  }

  /**
   * See {@link XorEncryptor#wrappedValue}.
   *
   * This provider does not have any wrapper objects, but does expect all
   * numbers to be in base 10, so this just converts the string from the radix
   * it's in to the specified radix.
   *
   * @return string
   *   The base-10 version of the number.
   */
  protected function wrappedValue($stringValue,
                                  $radix = XorEncryptor::DEFAULT_RADIX) {
    return base_convert($stringValue, $radix, 10);
  }

  /**
   * See {@link XorEncryptor#xorValue}.
   */
  protected function xorValue($value1, $value2) {
    return ($value1 ^ $value2);
  }

  /**
   * See {@link XorEncryptor#stringValue}.
   *
   * This provider does not have any wrapper objects, but does expect all
   * numbers to be in base 10, so this just converts the string from base 10 to
   * the radix it should be in.
   *
   * @return string
   *   The number in the requested radix.
   */
  protected function stringValue($value, $radix = XorEncryptor::DEFAULT_RADIX) {
    return base_convert($value, 10, $radix);
  }

  /**
   * Detects whether or not the current PHP installation is 32-bit or 64-bit.
   *
   * @return int
   *  Either 32 or 64.
   */
  protected function detectPlatformPrecision() {
    switch(PHP_INT_SIZE) {
      case 4:
        $result = 32;
        break;

      case 8:
      default:
        $result = 64;
        break;
    }
  }
}
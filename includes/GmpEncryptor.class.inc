<?php
/**
 * @file
 *   The GMP-based implementation of the exclusive-OR encryption provider.
 *
 * @author Guy Paddock (guy.paddock@redbottledesign.com)
 */
class GmpEncryptor extends XorEncryptor {
  /**
   * Initialize a new instance of GmpEncryptor for the specified number
   * base / radix, number of padding digits, and encryption key.
   *
   * @param integer $radix
   *   The "base" or radix of numbers (i.e. base-10, base-16, etc) that will be
   *   encrypted. If unset, the default of DEFAULT_RADIX will be a used.
   * @param integer $numPadDigits
   *   The number of random padding digits to add to an encrypted string to make
   *   it more difficult to derive the key. If unset, the default of
   *   DEFAULT_NUM_PAD_DIGITS will be a used.
   * @param number $key
   *   The key to use for encryption. If unset, a default of (PI * e) ^ 10 will
   *   be used.
   */
  public function __construct($radix = XorEncryptor::DEFAULT_RADIX,
                              $numPadDigits = XorEncryptor::DEFAULT_NUM_PAD_DIGITS,
                              $key = NULL) {
    parent::__construct($radix, $numPadDigits, $key);
  }

  /**
   * See {@link XorEncryptor#isSupported()}.
   */
  public function isSupported() {
    return extension_loaded('gmp');
  }

  /**
   * See {@link XorEncryptor#getName()}.
   */
  public function getName() {
    return t('GNU Multiple Precision');
  }

  /**
   * See {@link XorEncryptor#wrappedValue}.
   *
   * @return resource
   *   The GMP resource wrapper for the value.
   */
  protected function wrappedValue($stringValue,
                                  $radix = XorEncryptor::DEFAULT_RADIX) {
    return gmp_init($stringValue, $radix);
  }

  /**
   * See {@link XorEncryptor#xorValue}.
   */
  protected function xorValue($value1, $value2) {
    return gmp_xor($value1, $value2);
  }

  /**
   * See {@link XorEncryptor#stringValue}.
   */
  protected function stringValue($value, $radix = XorEncryptor::DEFAULT_RADIX) {
    return gmp_strval($value, $radix);
  }
}
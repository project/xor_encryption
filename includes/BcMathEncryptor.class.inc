<?php
/**
 * @file
 *   The BC Math-based implementation of the exclusive-OR encryption provider.
 *
 * @author Guy Paddock (guy.paddock@redbottledesign.com)
 */
class BcMathEncryptor extends XorEncryptor {
  /**
   * Initialize a new instance of BcMathEncryptor for the specified number
   * base / radix, number of padding digits, and encryption key.
   *
   * @param integer $radix
   *   The "base" or radix of numbers (i.e. base-10, base-16, etc) that will be
   *   encrypted. If unset, the default of DEFAULT_RADIX will be a used.
   * @param integer $numPadDigits
   *   The number of random padding digits to add to an encrypted string to make
   *   it more difficult to derive the key. If unset, the default of
   *   DEFAULT_NUM_PAD_DIGITS will be a used.
   * @param number $key
   *   The key to use for encryption. If unset, a default of (PI * e) ^ 10 will
   *   be used.
   */
  public function __construct($radix = XorEncryptor::DEFAULT_RADIX,
      $numPadDigits = XorEncryptor::DEFAULT_NUM_PAD_DIGITS,
      $key = NULL) {
    parent::__construct($radix, $numPadDigits, $key);
  }

  /**
   * See {@link XorEncryptor#isSupported()}.
   */
  public function isSupported() {
    return extension_loaded('bcmath');
  }

  /**
   * See {@link XorEncryptor#getName()}.
   */
  public function getName() {
    return t('BCMath');
  }

  /**
   * See {@link XorEncryptor#wrappedValue}.
   *
   * BCMath does not have any wrapper objects, but does expect all numbers to be
   * in base 10, so this just converts the string from the radix it's in to the
   * specified radix.
   *
   * @return string
   *   The base-10 version of the number.
   */
  protected function wrappedValue($stringValue,
                                  $radix = XorEncryptor::DEFAULT_RADIX) {
    return BcMathUtils::base2dec($stringValue, $radix);
  }

  /**
   * See {@link XorEncryptor#xorValue}.
   */
  protected function xorValue($value1, $value2) {
    return BcMathUtils::bcxor($value1, $value2);
  }

  /**
   * See {@link XorEncryptor#stringValue}.
   *
   * BCMath does not have any wrapper objects, but does expect all numbers to be
   * in base 10, so this just converts the string from base 10 to the radix it
   * should be in.
   *
   * @return string
   *   The number in the requested radix.
   */
  protected function stringValue($value, $radix = XorEncryptor::DEFAULT_RADIX) {
    return BcMathUtils::dec2base($value, $radix);
  }
}
<?php
/**
 * @file
 * An exclusive-OR encryption provider. This class provides the ability to use
 * exclusive-OR (XOR) operations and random padding to encrypt and decrypt
 * values.
 *
 * When encrypting a value, the value is randomly padded and then
 * exclusively-OR-ed with an encryption key to produce the encrypted value. When
 * decrypting an encrypted value, the encrypted value is exclusively-OR-ed with
 * the same encryption key, and then stripped of the padding digits, to produce
 * the original, un-encrypted value.
 *
 * During either process, if a value has a longer bit-length than the encryption
 * key, the encryption key is automatically expanded to match the bit-length of
 * the value.
 *
 * @author Guy Paddock (guy.paddock@redbottledesign.com)
 */
abstract class XorEncryptor {
  /**
   * The default "base" or radix of numbers (i.e. base-10, base-16, etc) that
   * will be encrypted.
   */
  const DEFAULT_RADIX = 10;

  /**
   * The default number of random padding digits to add to an encrypted string
   * to make it more difficult to derive the key.
   */
  const DEFAULT_NUM_PAD_DIGITS = 3;

  /**
   * The "base" or radix of numbers (i.e. base-10, base-16, etc) that will be
   * encrypted.
   *
   * @var number
   */
  private $radix;

  /**
   * The number of random padding digits to add to an encrypted string to make
   * it more difficult to derive the key.
   *
   * @var integer
   */
  private $numPadDigits;

  /**
   * The key to use for encryption.
   *
   * @var number
   */
  private $key;

  /**
   * Initialize a new instance of XorEncryptor for the specified number base /
   * radix, number of padding digits, and encryption key.
   *
   * @param integer $radix
   *   The "base" or radix of numbers (i.e. base-10, base-16, etc) that will be
   *   encrypted. If unset, the default of DEFAULT_RADIX will be a used.
   * @param integer $numPadDigits
   *   The number of random padding digits to add to an encrypted string to make
   *   it more difficult to derive the key. If unset, the default of
   *   DEFAULT_NUM_PAD_DIGITS will be a used.
   * @param number $key
   *   The key to use for encryption. If unset, a default of (PI * e) ^ 10 will
   *   be used.
   */
  public function __construct($radix = XorEncryptor::DEFAULT_RADIX,
      $numPadDigits = XorEncryptor::DEFAULT_NUM_PAD_DIGITS,
      $key = NULL) {
    $this->radix = $radix;
    $this->numPadDigits = $numPadDigits;

    if ($this->isSupported()) {
      // Provide default key, if necessary.
      if ($key === NULL) {
        $this->key = $this->wrappedValue((int)pow(pi() * exp(1), 10), 10);
      }
      else {
        $this->key = $this->wrappedValue($key, $this->radix);
      }
    }
  }

  /**
   * Indicates whether or not this provider is supported in the current
   * PHP installation.
   *
   * Consumers of this class must check that the provider is supported
   * before attempting to use it.
   *
   * @return boolean
   */
  public abstract function isSupported();

  /**
   * Gets the name of this provider, for display to the user.
   *
   * @return string
   */
  public abstract function getName();

  /**
   * Encrypt the specified value.
   *
   * @param number $value
   *   The value to encrypt.
   *
   * @return string
   *   The encrypted form of the value.
   *
   * @throws LogicException
   *   If this provider is not supported on the current PHP installation.
   */
  public function encrypt($value) {
    $this->ensureSupported();

    $resourceValue = $this->wrappedValue($value, $this->radix);
    $padded = $this->addRandomPadding($resourceValue);
    $expandedKey = $this->calculateExpandedKeyFor($padded);
    $encrypted = $this->xorValue($padded, $expandedKey);

    // Return the result as a string.
    return $this->stringValue($encrypted, $this->radix);
  }

  /**
   * Decrypt the specified value.
   *
   * @param string $value
   *   The value to decrypt.
   *
   * @return number
   *   The decrypted form of the value.
   *
   * @throws LogicException
   *   If this provider is not supported on the current PHP installation.
   */
  public function decrypt($value) {
    $this->ensureSupported();

    $resourceValue = $this->wrappedValue($value, $this->radix);
    $expandedKey = $this->calculateExpandedKeyFor($resourceValue);
    $decrypted = $this->xorValue($resourceValue, $expandedKey);
    $withoutPadding = $this->removeRandomPadding($decrypted);

    // Return the result as a string.
    return $this->stringValue($withoutPadding, $this->radix);
  }

  /**
   * Initializes the seed used for applying random padding to numbers.
   *
   * This has no effect if random padding is not being applied during
   * encryption.
   *
   * @param int $seed
   *   The seed value.
   */
  public function setRandomSeed($seed) {
    mt_srand($seed);
  }

  /**
   * Wraps a number in the appropriate object for the underlying library.
   *
   * The number should be provided as a string to ensure proper precision.
   * An optional radix can be provided to indicate how the digits in the string
   * are to be interpreted.
   *
   * @param string $stringValue
   *   A string containing the digits of the number to wrap.
   * @param int $radix
   *   An optional parameter for the radix (i.e. base) of the number.
   *
   * @return mixed
   *   The expanded-precision, wrapped number.
   */
  protected abstract function wrappedValue($stringValue, $radix = XorEncryptor::DEFAULT_RADIX);

  /**
   * Computes the XOR of two numbers using the underlying library.
   *
   * Both numbers must be provided as wrapped objects.
   *
   * @param mixed $value1
   *   The first number, wrapped in the underlying library's wrapper.
   * @param mixed $value2
   *   The second number, wrapped in the underlying library's wrapper.
   *
   * @return mixed
   *   The high-precision result.
   */
  protected abstract function xorValue($value1, $value2);

  /**
   * Converts a wrapped object to its string representation.
   *
   * The number must be a wrapped object. An optional radix can be provided to
   * indicate how the number should be converted into digits.
   *
   * @param mixed $value
   *   A wrapped value.
   * @param int $radix
   *   An optional parameter for the radix (i.e. base) of the number.
   *
   * @return string
   *   The number, as a string in the provided radix.
   */
  protected abstract function stringValue($value, $radix = XorEncryptor::DEFAULT_RADIX);

  /**
   * Ensures that this provider is supported on this PHP installation.
   *
   * This should be called before any operations that require the underlying
   * library.
   */
  protected function ensureSupported() {
    if (!$this->isSupported()) {
      throw new LogicException(
        t('This implementation is not supported on this system.'));
    }
  }

  /**
   * Add random padding digits to the specified value.
   *
   * @param resource $value
   *   The GMP resource that represents the value to which random padding will
   *   be added.
   *
   * @return resource
   *   The GMP resource that represents $value with random padding added.
   */
  private function addRandomPadding($value) {
    $temp  = '';

    for ($i = 0; $i < $this->numPadDigits; ++$i) {
      /* Pick a random value in the range of the radix and convert it to a GMP
       * resource.
       */
      $randomDigit = $this->wrappedValue(mt_rand(1, $this->radix - 1), 10);

      /* Convert the random digit to the appropriate base and append it to the
       * output string.
       */
      $temp .= $this->stringValue($randomDigit, $this->radix);
    }

    // Now, append the original value.
    $temp .= $this->stringValue($value, $this->radix);

    // Return the result as a GMP resource in the appropriate base.
    return $this->wrappedValue($temp, $this->radix);
  }

  /**
   * Remove random padding digits from the specified value.
   *
   * @param resource $value
   *   The GMP resource that represents the value from which random padding will
   *   be removed.
   *
   * @return resource
   *   The GMP resource that represents $value with random padding removed.
   */
  private function removeRandomPadding($value) {
    // Convert the resource to the appropriate base.
    $withPad  = $this->stringValue($value, $this->radix);
    $withoutPad = '0';

    if ($this->numPadDigits < strlen($withPad)) {
      $withoutPad = substr($withPad, $this->numPadDigits);
    }

    // Return the result as a GMP resource in the appropriate base.
    return $this->wrappedValue($withoutPad, $this->radix);
  }

  /**
   * Calculate a version of the current key that has been expanded to match the
   * length (in bits) of the specified value, for encrypting or decrypting
   * values longer than the original key.
   *
   * @param resource $value
   *   A GMP resource that represents the value that the key must match in
   *   bit-length.
   *
   * @return resource
   *   A GMP resource that represents the key value of the necessary bit length.
   */
  private function calculateExpandedKeyFor($value) {
    $valueBits        = $this->stringValue($value, 2);
    $desiredBitLength = strlen($valueBits);

    $keyBits      = $this->stringValue($this->key, 2);
    $keyBitLength = strlen($keyBits);

    /* The prefix bit is always the complement of the first bit of the value
     * we're encrypting, to ensure that when the encryption key and the value
     * are XORed, the resulting value will be of a predictable length.
     */
    $prefixBit = ($valueBits[0] == 0) ? '1' : '0';

    $prefixedKeyBits      = $prefixBit . $keyBits;
    $prefixedKeyBitLength = $keyBitLength + 1;

    $newKeyBits = $prefixedKeyBits;

    for ($bitIndex = 0; $bitIndex < ($desiredBitLength - $prefixedKeyBitLength); ++$bitIndex) {
      // Reverse the key during expansion.
      $newKeyBits .= $keyBits[$keyBitLength - ($bitIndex % $keyBitLength) - 1];
    }

    return $this->wrappedValue($newKeyBits, 2);
  }
}
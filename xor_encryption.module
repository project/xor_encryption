<?php
/**
 * @file
 *   Module file for the "XOR Encryption API" module.
 *
 * @author Guy Paddock (guy.paddock@redbottledesign.com)
 */

/**
 * Constant for the name of this module.
 *
 * @var string
 */
define('XOEN_MODULE_NAME', 'xor_encryption');

/**
 * Encrypts the specified value in the specified radix/base, using the
 * specified number of padding digits, and the specified encryption key.
 *
 * This function wraps the functionality of the XorEncryptor class.
 *
 * @param number $value
 *   The value to be encrypted.
 * @param integer $radix
 *   An optional radix or base of the number. For example, 10 for decimal
 *   numbers, or 16 for hexadecimal numbers. If not specified, the default
 *   decimal radix of 10 will be used.
 * @param integer $num_pad_digits
 *   An optional number of digits of padding to add to the value before
 *   encrypting it. If not specified, the default of three padding digits will
 *   be used.
 * @param number $key
 *   An optional encryption key to use. If not specified, the default key of
 *   (PI * e) ^ 10 will be used.
 * @param int $random_seed
 *   An optional seed to use for the random padding. If not specified, the seed
 *   is initialized automatically (likely based on the system time).
 *
 * @return string
 *   The encrypted value.
 */
function xor_encryption_encrypt($value, $radix = NULL, $num_pad_digits = NULL,
                                $key = NULL, $random_seed = NULL) {
  $encryptor = _xor_encryption_get_provider($radix, $num_pad_digits, $key);

  if (!empty($encryptor)) {
    if ($random_seed !== NULL) {
      $encryptor->setRandomSeed($random_seed);
    }

    $result = $encryptor->encrypt($value);
  }
  else {
    $result = NULL;
  }

  return $result;
}

/**
 * Decrypts the specified value in the specified radix/base, using the
 * specified number of padding digits, and the specified encryption key.
 *
 * This function wraps the functionality of the XorEncryptor class.
 *
 * @param string $value
 *   The value to be decrypted.
 * @param integer $radix
 *   The radix or base of the number. For example, 10 for decimal numbers, or 16
 *   for hexadecimal numbers. If not specified, the default decimal radix of 10
 *   will be used.
 * @param integer $num_pad_digits
 *   The number of digits of padding to remove from the value before decrypting
 *   it. If not specified, the default of three padding digits will be used.
 * @param number $key
 *   The encryption key that was used to encrypt the value. If not specified,
 *   the default key of (PI * e) ^ 10 will be used.
 *
 * @return number
 *   The decrypted value.
 */
function xor_encryption_decrypt($value, $radix = NULL, $num_pad_digits = NULL,
                                $key = NULL) {
  $decryptor = _xor_encryption_get_provider($radix, $num_pad_digits, $key);

  if (!empty($decryptor)) {
    $result = $decryptor->decrypt($value);
  }
  else {
    $result = NULL;
  }

  return $result;
}

/**
 * Implementation of {@link hook_requirements()}.
 */
function xor_encryption_requirements($phase) {
  $requirements = array();

  // Ensure translations don't break during installation.
  $t = get_t();

  $requirements[XOEN_MODULE_NAME] = array(
    'title' => t('XOR Encryption Provider'),
  );

  $encryption_reqs =& $requirements[XOEN_MODULE_NAME];
  $provider        = _xor_encryption_get_provider();

  if (empty($provider)) {
    $encryption_reqs['value']       = t('No providers available');
    $encryption_reqs['severity']    = REQUIREMENT_ERROR;
    $encryption_reqs['description'] =
      t('XOR encryption cannot function without an implementation. Either the GMP or BC Math PHP extension must be installed.');
  }
  else {
    if ($phase == 'runtime') {
      $encryption_reqs['value'] = $provider->getName();
    }

    $encryption_reqs['severity'] = REQUIREMENT_OK;
  }

  return $requirements;
}

/**
 * Gets the appropriate provider of XOR encryption based on the PHP installation.
 *
 * @param integer $radix
 *   The radix or base of the number. For example, 10 for decimal numbers, or 16
 *   for hexadecimal numbers. If not specified, the default decimal radix of 10
 *   will be used.
 * @param integer $num_pad_digits
 *   The number of digits of padding to remove from the value before decrypting
 *   it. If not specified, the default of three padding digits will be used.
 * @param number $key
 *   The encryption key that was used to encrypt the value. If not specified,
 *   the default key of (PI * e) ^ 10 will be used.
 *
 * @return XorEncryptor
 *   Either an XOR encryption provider, or NULL if no providers are available.
 */
function _xor_encryption_get_provider($radix = NULL, $num_pad_digits = NULL, $key = NULL) {
  $result = NULL;

  if ($radix === NULL)
    $radix = XorEncryptor::DEFAULT_RADIX;

  if ($num_pad_digits === NULL)
    $num_pad_digits = XorEncryptor::DEFAULT_NUM_PAD_DIGITS;

  $providers = array(
    new GmpEncryptor($radix, $num_pad_digits, $key),
    new BcMathEncryptor($radix, $num_pad_digits, $key),
    new NativePhpEncryptor($radix, $num_pad_digits, $key),
  );

  foreach ($providers as $provider) {
    /* @var XorEncryptor $provider */
    if ($provider->isSupported()) {
      $result = $provider;
      break;
    }
  }

  return $result;
}